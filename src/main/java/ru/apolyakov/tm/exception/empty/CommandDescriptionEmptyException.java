package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class CommandDescriptionEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Command description cannot be empty.";

    public CommandDescriptionEmptyException() {
        super(MESSAGE);
    }

}
