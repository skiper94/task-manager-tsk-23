package ru.apolyakov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class UserSelfLockNotAllowedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Cannot lock logged-on user.";

    public UserSelfLockNotAllowedException() {
        super(MESSAGE);
    }

}
