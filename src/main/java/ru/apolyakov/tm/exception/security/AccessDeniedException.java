package ru.apolyakov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Access denied!";

    public AccessDeniedException() {
        super(MESSAGE);
    }

}
