package ru.apolyakov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class UserExistsWithLoginException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already exists with login: ";

    public UserExistsWithLoginException(final String login) {
        super(MESSAGE + login);
    }

}
