package ru.apolyakov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User not found.";

    public UserNotFoundException() {
        super(MESSAGE);
    }

}
