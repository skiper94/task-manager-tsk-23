package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-start-by-id";

    @NotNull
    private final static String DESCRIPTION = "Start project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = TerminalUtil.readLine(ID_INPUT);
        throwExceptionIfNull(getProjectService().startById(id));
    }

}
