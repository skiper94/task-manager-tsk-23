package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectStartByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-start-by-name";

    @NotNull
    private final static String DESCRIPTION = "Start project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = TerminalUtil.readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectService().startByName(name));
    }

}
