package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-remove-by-name";

    @NotNull
    private final static String DESCRIPTION = "Removing project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = TerminalUtil.readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectByName(name));
    }

}
