package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;
import static ru.apolyakov.tm.util.TerminalUtil.readRole;

public final class UserAddCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "registration";

    @NotNull
    private static final String DESCRIPTION = "Adding a new user";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine("Enter email: ");
        @NotNull final Role role = readRole(ENTER_ROLE);
        @NotNull final String lastName = readLine("Enter last name: ");
        @NotNull final String firstName = readLine("Enter first name: ");
        @NotNull final String middleName = readLine("Enter middle name: ");
        getUserService().add(login, password, email, role, firstName, middleName, lastName);
    }

}
