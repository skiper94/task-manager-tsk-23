package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserUnlockByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-unlock-by-id";

    @NotNull
    private static final String DESCRIPTION = "Unlocking user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (!getUserService().unlockById(id)) printLinesWithEmptyLine(USER_IS_NOT_LOCKED);
    }

}
