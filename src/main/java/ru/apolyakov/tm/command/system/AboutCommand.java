package ru.apolyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.command.AbstractCommand;

import static ru.apolyakov.tm.constant.StringConst.APP_ABOUT;
import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "about";

    @NotNull
    private final static String ARG_NAME = "-a";

    @NotNull
    private final static String DESCRIPTION = "Show developer info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(APP_ABOUT);
    }

}
