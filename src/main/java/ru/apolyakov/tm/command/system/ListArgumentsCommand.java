package ru.apolyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.command.AbstractCommand;

import static ru.apolyakov.tm.util.TerminalUtil.printConfirmCommand;

public final class ListArgumentsCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "commands-list";

    @NotNull
    private final static String DESCRIPTION = "List available commands";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        printConfirmCommand(DESCRIPTION);
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            System.out.println(command.getCommand());
        }
        System.out.println();
    }

}
