package ru.apolyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.command.AbstractCommand;

import static ru.apolyakov.tm.constant.StringConst.APP_VERSION;
import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;

public class VersionCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "version";

    @NotNull
    private final static String ARG_NAME = "-v";

    @NotNull
    private final static String DESCRIPTION = "Show version info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(APP_VERSION);
    }

}
