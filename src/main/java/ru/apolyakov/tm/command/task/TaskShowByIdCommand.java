package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Task;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-show-by-ud";

    @NotNull
    private final static String DESCRIPTION = "Showing task by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @Nullable final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

}
