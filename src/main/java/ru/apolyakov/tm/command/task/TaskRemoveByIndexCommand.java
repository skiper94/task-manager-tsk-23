package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readNumber;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-remove-by-index";

    @NotNull
    private final static String DESCRIPTION = "Removing task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getTaskService().removeOneByIndex(index - 1));
    }

}
