package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.apolyakov.tm.util.TerminalUtil.*;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "tasks-list-by-project-id";

    @NotNull
    private final static String DESCRIPTION = "Showing all tasks by Project ID";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void execute() {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final Comparator<Task> taskComparator = readComparator();
        @NotNull final List<Task> taskList = getProjectTaskService().findAllTasksByProjectId(projectId, taskComparator);
        if (taskList.isEmpty()) {
            printLinesWithEmptyLine("No tasks yet. Type <add task to project> to add a task to a project.");
            return;
        }
        printListWithIndexes(taskList);
    }

}
