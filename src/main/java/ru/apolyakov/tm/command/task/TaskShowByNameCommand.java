package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Task;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-show-by-name";

    @NotNull
    private final static String DESCRIPTION = "Showing task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @Nullable final Task task = getTaskService().findOneByName(name);
        showTask(task);
    }

}
