package ru.apolyakov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.ServiceLocator;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.ICommandService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.other.ServiceLocatorNotFoundException;

import java.util.Optional;

import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

public abstract class AbstractCommand {

    @NotNull
    public static final String ID_INPUT = "Please enter id: ";

    @NotNull
    public static final String INDEX_INPUT = "Please enter index: ";

    @NotNull
    public static final String NAME_INPUT = "Please enter name: ";

    @NotNull
    public static final String DESCRIPTION_INPUT = "Please enter description: ";

    @Nullable
    protected ServiceLocator serviceLocator;

    protected boolean needAuthorization = false;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    protected IAuthService getAuthService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getAuthService();
    }

    @NotNull
    protected ICommandService getCommandService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getCommandService();
    }

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        return (getCommand() + (isEmptyStr(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    public boolean getNeedAuthorization() {
        return needAuthorization;
    }

    public void setNeedAuthorization(final boolean needAuthorization) {
        this.needAuthorization = needAuthorization;
    }
}
