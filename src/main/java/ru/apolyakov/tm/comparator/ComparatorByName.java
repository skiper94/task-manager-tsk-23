package ru.apolyakov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.entity.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    @NotNull
    private static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    @NotNull
    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public final int compare(@Nullable final IHasName o1, @Nullable final IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
