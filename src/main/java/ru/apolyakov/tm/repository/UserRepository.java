package ru.apolyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractModelRepository<User> implements IUserRepository {

    @NotNull
    public final Predicate<User> predicateByEmail(final String email) {
        return s -> email.equals(s.getEmail());
    }

    @NotNull
    public final Predicate<User> predicateByLogin(final String login) {
        return s -> login.equals(s.getLogin());
    }

    @Nullable
    @Override
    public final User findByEmail(@NotNull final String email) {
        return entities.values().stream()
                .filter(predicateByEmail(email))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public final User findOneById(@NotNull final String id) {
        return entities.get(id);
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        return entities.values().stream()
                .filter(predicateByLogin(login))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public final String findIdByLogin(@NotNull final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        return user.map(User::getId).orElse(null);
    }

    @Override
    public final boolean isEmpty() {
        return entities.isEmpty();
    }

    @Override
    public final boolean isFoundByEmail(@NotNull final String email) {
        return (findByEmail(email)) != null;
    }

    @Override
    public final boolean isFoundByLogin(@NotNull final String login) {
        return (findByLogin(login)) != null;
    }

    @Override
    public final void remove(@NotNull final User user) {
        entities.remove(user.getId());
    }

    @Override
    public final void removeByLogin(@NotNull final String login) {
        Optional.ofNullable(findByLogin(login)).ifPresent(this::remove);
    }

    @Nullable
    @Override
    public final User update(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                             @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        @NotNull final Optional<User> user = Optional.ofNullable(findOneById(id));
        user.ifPresent(u -> {
            u.setLogin(login);
            u.setPasswordHash(password);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
            u.setLastName(lastName);
        });
        return user.orElse(null);
    }

}
