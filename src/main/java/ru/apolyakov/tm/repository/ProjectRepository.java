package ru.apolyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {

    @Override
    public final void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Project project = new Project(name, description, userId);
        entities.put(project.getId(), project);
    }

}
