package ru.apolyakov.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.ServiceLocator;
import ru.apolyakov.tm.api.repository.*;
import ru.apolyakov.tm.api.service.*;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.command.project.*;
import ru.apolyakov.tm.command.system.*;
import ru.apolyakov.tm.command.task.*;
import ru.apolyakov.tm.command.user.*;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.AbstractException;
import ru.apolyakov.tm.exception.empty.CommandDescriptionEmptyException;
import ru.apolyakov.tm.exception.empty.CommandNameEmptyException;
import ru.apolyakov.tm.exception.entity.CommandNotFoundException;
import ru.apolyakov.tm.repository.*;
import ru.apolyakov.tm.service.*;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.Objects;
import java.util.Optional;

import static ru.apolyakov.tm.constant.StringConst.*;
import static ru.apolyakov.tm.util.TerminalUtil.printConfirmCommand;
import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

@Getter
public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, authService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService, authService);

    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyStr(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        printConfirmCommand(command.getDescription());
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command.getNeedAuthorization()) authService.throwExceptionIfNotAuthorized();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    public void initCommands() {
        registerCommand(new TaskListCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskShowByNameCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskRemoveByNameCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskStartByNameCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCompleteByNameCommand());
        registerCommand(new TaskListByProjectCommand());
        registerCommand(new TaskAddToProjectCommand());
        registerCommand(new TaskRemoveFromProjectCommand());

        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectShowByNameCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectRemoveByNameCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectStartByNameCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCompleteByNameCommand());

        registerCommand(new LoginCommand());
        registerCommand(new LogoutCommand());
        registerCommand(new UserListCommand());
        registerCommand(new UserAddCommand());
        registerCommand(new UserShowByIdCommand());
        registerCommand(new UserShowByLoginCommand());
        registerCommand(new UserUpdateByIdCommand());
        registerCommand(new UserUpdateByLoginCommand());
        registerCommand(new ChangePasswordCommand());
        registerCommand(new ChangeRoleCommand());
        registerCommand(new UserRemoveByIdCommand());
        registerCommand(new UserRemoveByLoginCommand());
        registerCommand(new UserLockByIdCommand());
        registerCommand(new UserLockByLoginCommand());
        registerCommand(new UserUnlockByIdCommand());
        registerCommand(new UserUnlockByLoginCommand());
        registerCommand(new ShowMyProfile());

        registerCommand(new InfoCommand());
        registerCommand(new AboutCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new ListCommandsCommand());
        registerCommand(new ListArgumentsCommand());
        registerCommand(new ExitCommand());
    }

    private void initData() {
        try {
            userService.add("admin", "admin", "apolyakov@tsconsalting.com", Role.ADMIN, "Andrey", "Dmitrievich", "Polyakov");
            userService.add("user", "user", "user@tsconsalting.com", Role.USER, "Ivan", "Ivanovich", "Ivanov");

            authService.login("admin", "admin");

            taskService.add("Task #1", "Description of task #1");
            taskService.add("Task #2", "Description of task #2");
            taskService.add("Task #3", "Description of task #3");
            taskService.add("Task #4", "Description of task #4");
            taskService.add("Task #5", "Description of task #5");
            taskService.startByName("Task #2");
            taskService.completeByName("Task #3");

            projectService.add("Project #1", "Description of Project #1");
            projectService.add("Project #2", "Description of Project #2");
            projectService.add("Project #3", "Description of Project #3");
            projectService.add("Project #4", "Description of Project #4");
            projectService.add("Project #5", "Description of Project #5");

            projectTaskService.addTaskToProject(Objects.requireNonNull(projectService.findOneByName("Project #4")).getId(), Objects.requireNonNull(taskService.findOneByName("Task #4")).getId());

            authService.logout();
            authService.login("user", "user");

            taskService.add("Task #6", "Description of task #6");
            taskService.add("Task #7", "Description of task #7");

            projectService.add("Project #6", "Description of Project #6");
            projectService.add("Project #7", "Description of Project #7");

            projectTaskService.addTaskToProject(Objects.requireNonNull(projectService.findOneByName("Project #6")).getId(), Objects.requireNonNull(taskService.findOneByName("Task #7")).getId());

            authService.logout();

        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        initData();
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyStr(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                printConfirmCommand(APP_COMMAND_SUCCESS);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {
            @NotNull final String terminalCommand = command.getCommand();
            @NotNull final String commandDescription = command.getDescription();
            @Nullable final String commandArgument = command.getArgument();
            if (isEmptyStr(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyStr(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyStr(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initCommands();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
