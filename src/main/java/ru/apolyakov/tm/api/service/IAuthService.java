package ru.apolyakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.enumerated.Role;

public interface IAuthService {

    @Nullable String getUserId();

    void setUserId(@Nullable String userId);

    @Nullable
    String getUserLogin();

    void checkRoles(@NotNull Role[] roles);

    boolean isNoUserLoggedIn();

    boolean isPrivilegedUser();

    void login(@NotNull String login, @NotNull String password);

    void logout();

    void throwExceptionIfNotAuthorized();

}
