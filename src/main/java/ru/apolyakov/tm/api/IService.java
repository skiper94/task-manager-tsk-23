package ru.apolyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    void add(@NotNull E entity);

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@NotNull String id);

    int getSize();

    boolean isEmpty();

    @Nullable
    E removeOneById(@NotNull String id);
}
