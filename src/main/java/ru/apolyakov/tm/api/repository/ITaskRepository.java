package ru.apolyakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<Task> {

    @Nullable Task addTaskToProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable Task addTaskToProjectLimitByUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId, @NotNull Comparator<Task> comparator);

    @NotNull List<Task> findAllByProjectIdLimitByUser(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectIdLimitByUser(@NotNull String userId, @NotNull String projectId, @NotNull Comparator<Task> comparator);

    boolean isNotFoundTaskInProject(@NotNull String taskId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String projectId);

    @Nullable Task removeTaskFromProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable Task removeTaskFromProjectLimitByUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

}
