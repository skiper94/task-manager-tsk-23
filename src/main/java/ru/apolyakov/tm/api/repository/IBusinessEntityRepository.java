package ru.apolyakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void add(@NotNull String userId, @NotNull String name, @Nullable String description);

    void clear();

    void clearLimitByUser(@NotNull String currentUserId);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAllLimitByUser(@NotNull String userId);

    @NotNull
    List<E> findAllLimitByUser(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findOneByIdLimitByUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(int index);

    @Nullable
    E findOneByIndexLimitByUser(@NotNull String userId, int index);

    @Nullable
    E findOneByName(@NotNull String name);

    @Nullable
    E findOneByNameLimitByUser(@NotNull String userId, @NotNull String name);

    @NotNull
    List<String> findKeysLimitByUser(@NotNull String userId);

    @Nullable
    String getId(@NotNull String name);

    @Nullable
    String getId(int index);

    @Nullable
    String getIdLimitByUser(@NotNull String userId, int index);

    @Nullable
    String getIdLimitByUser(@NotNull String userId, String name);

    int getSizeLimitByUser(@NotNull String userId);

    boolean isEmptyLimitByUser(@NotNull String userId);

    boolean isNotFoundById(@NotNull String id);

    boolean isNotFoundByIdLimitByUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIdLimitByUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIndex(int index);

    @Nullable
    E removeOneByIndexLimitByUser(@NotNull String userId, int index);

    @Nullable
    E removeOneByName(@NotNull String name);

    @Nullable
    E removeOneByNameLimitByUser(@NotNull String userId, @NotNull String name);

    @Nullable
    E update(@NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateOneLimitByUser(@NotNull String userId, @NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateStatusById(@NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIdLimitByUser(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIndex(int index, @NotNull Status status);

    @Nullable
    E updateStatusByIndexLimitByUser(@NotNull String userId, int index, @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String name, @NotNull Status status);

    @Nullable
    E updateStatusByNameLimitByUser(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
