package ru.apolyakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.command.AbstractCommand;

import java.util.Map;

public interface ICommandRepository {

    @NotNull Map<String, AbstractCommand> getArguments();

    @NotNull Map<String, AbstractCommand> getCommands();

}
