package ru.apolyakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.entity.IWBS;

@NoArgsConstructor
@Getter
@Setter
public final class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId;

    public Task(@NotNull final String name, @NotNull final String userId) {
        super(name, userId);
    }

    public Task(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

}
