package ru.apolyakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.entity.IWBS;
import ru.apolyakov.tm.enumerated.Status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ru.apolyakov.tm.constant.StringConst.EMPTY;
import static ru.apolyakov.tm.constant.StringConst.NEW_LINE;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractModel implements IWBS {

    @NotNull
    private String userId;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @NotNull
    private Status status = Status.NOT_STARTED;


    public AbstractBusinessEntity(@NotNull final String name, @NotNull final String userId) {
        this.name = name;
        this.userId = userId;
    }

    public AbstractBusinessEntity(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        this.name = name;
        this.description = description != null ? description : EMPTY;
        this.userId = userId;
    }

    @Override
    public String toString() {
        @NotNull final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return "ID: " + getId() + NEW_LINE +
                "Name: " + getName() + NEW_LINE +
                "Description: " + getDescription() + NEW_LINE +
                "Created: " + dateFormat.format(getCreated()) + NEW_LINE +
                "Status: " + getStatus() +
                (dateStart != null ? NEW_LINE + "Date start: " + dateFormat.format(dateStart) : "") +
                (dateFinish != null ? NEW_LINE + "Date finish: " + dateFormat.format(dateFinish) : "");


    }

}
