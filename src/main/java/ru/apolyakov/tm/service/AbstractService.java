package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.exception.empty.IdEmptyException;
import ru.apolyakov.tm.model.AbstractModel;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.List;

public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@NotNull final E entity) {
        repository.add(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    public boolean isEmpty() {
        return repository.isEmpty();
    }

}
