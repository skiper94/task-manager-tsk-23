package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IAuthRepository;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.empty.LoginEmptyException;
import ru.apolyakov.tm.exception.empty.PasswordEmptyException;
import ru.apolyakov.tm.exception.entity.UserNotFoundException;
import ru.apolyakov.tm.exception.entity.UserNotLoggedInException;
import ru.apolyakov.tm.exception.incorrect.IncorrectCredentialsException;
import ru.apolyakov.tm.exception.security.AccessDeniedException;
import ru.apolyakov.tm.exception.security.AccessDeniedNotAuthorizedException;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IAuthRepository authRepository;

    @NotNull
    private final IUserService userService;

    public AuthService(@NotNull final IAuthRepository authRepository, @NotNull final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Nullable
    @Override
    public final String getUserId() {
        return authRepository.getUserId();
    }

    @Override
    public final void setUserId(@Nullable final String currentUserId) {
        authRepository.setUserId(currentUserId);
    }

    @NotNull
    @Override
    public final String getUserLogin() {
        if (ValidationUtil.isEmptyStr(getUserId())) throw new AccessDeniedNotAuthorizedException();
        @NotNull final User user = Optional.ofNullable(userService.findOneById(getUserId())).orElseThrow(UserNotFoundException::new);
        return user.getLogin();
    }

    @Override
    public final void checkRoles(@NotNull final Role[] roles) {
        if (roles == null) return;
        if (ValidationUtil.isEmptyStr(getUserId())) return;
        @NotNull final User user = Optional.ofNullable(userService.findOneById(getUserId())).orElseThrow(UserNotFoundException::new);
        for (@NotNull final Role role : roles) {
            if (user.getRole().equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public final boolean isNoUserLoggedIn() {
        return ValidationUtil.isEmptyStr(authRepository.getUserId());
    }

    @Override
    public final boolean isPrivilegedUser() {
        if (ValidationUtil.isEmptyStr(getUserId())) throw new AccessDeniedNotAuthorizedException();
        @NotNull final User user = Optional.ofNullable(userService.findOneById(getUserId())).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    public final void login(@NotNull final String login, @NotNull final String password) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        if (ValidationUtil.isEmptyStr(password)) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new IncorrectCredentialsException();
        if (user.isLocked()) throw new AccessDeniedException();
        setUserId(user.getId());
    }

    @Override
    public final void logout() {
        if (isNoUserLoggedIn()) throw new UserNotLoggedInException();
        setUserId(null);
    }

    @Override
    public final void throwExceptionIfNotAuthorized() {
        if (isNoUserLoggedIn()) throw new AccessDeniedNotAuthorizedException();
    }

}
